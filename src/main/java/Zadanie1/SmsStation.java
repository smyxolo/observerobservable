package Zadanie1;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class SmsStation{

    Executor antennas = Executors.newFixedThreadPool(8);
    List<Phone> phoneBook = new ArrayList<>();

    public void addPhone(Phone phone){
        phoneBook.add(phone);
    }

    public void sendSms(int number, String message){
        SmsRequest sr = new SmsRequest(message, number);
        for (Phone phone: phoneBook) {
            sr.addObserver(phone);
        }
        antennas.execute(sr);

    }
}
