package Zadanie1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        SmsStation ss = new SmsStation();
        ss.addPhone(new Phone(1234));
        ss.addPhone(new Phone(2345));
        ss.addPhone(new Phone(3456));
        ss.addPhone(new Phone(4567));
        ss.addPhone(new Phone(5678));

        Scanner skan = new Scanner(System.in);
        boolean isWorking = true;
        while (isWorking){
            System.out.println("Enter phone number (1234/2345/3456/4567/5678) and text:");
            String inputLine = skan.nextLine();
            int number = Integer.parseInt(inputLine.split(" ", 2)[0]);
            System.out.println("Enter your message content:");
            String text = inputLine.split(" ", 2)[1];
            ss.sendSms(number, text);

            System.out.println();
        }
    }
}
