package Zadanie1;

import lombok.Getter;

@Getter

public class Message implements Runnable{
    private String text;
    private int number;

    public Message(String text, int number) {
        this.text = text;
        this.number = number;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(1000*text.length());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
