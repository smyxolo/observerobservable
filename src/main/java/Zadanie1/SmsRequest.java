package Zadanie1;

import lombok.Getter;

import java.util.Observable;

@Getter

public class SmsRequest extends Observable implements Runnable {

    private Message message;

    public SmsRequest(String text, int number) {
        this.message = new Message(text, number);
    }

    @Override
    public void run() {
        try {
            System.out.println("Sending message " + message.getText());
            Thread.sleep(200 * message.getText().length());
            super.setChanged();
            super.notifyObservers(message);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}
