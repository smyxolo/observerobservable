package Zadanie4;


import java.util.Observable;

public class NewsStation extends Observable {

    public NewsStation() {
    }

    public void sendMessage(String message){
        super.setChanged();
        super.notifyObservers(message);
    }
}
