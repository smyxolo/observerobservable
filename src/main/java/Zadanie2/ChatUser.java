package Zadanie2;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

@Getter
@Setter

public class ChatUser implements Observer {
    private int id;
    private String nick;
    private List<String> messages = new ArrayList<>();
    private boolean isAdmin;

    public ChatUser(String nick) {
        this.nick = nick;
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof Message) {
            if ((!(((Message) arg).isPrivate()) && ((Message) arg).getSenderID() != this.id)
                    || ((Message) arg).isPrivate() && ((Message) arg).getReceiverID() == this.id) {
                System.out.println("User " + this.nick + " has received message: '" + ((Message) arg).getText() + "'");
                messages.add(((Message) arg).getText());
            }
        }
        else if(((Message) arg).getSenderID() != this.id) System.out.println("Wrong message type.");
    }

    @Override
    public String toString() {
        return nick;
    }
}
