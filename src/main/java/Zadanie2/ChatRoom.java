package Zadanie2;

import java.util.*;

//todo implement kicking

public class ChatRoom extends Observable {
    Map<Integer, ChatUser> onlineUsers = new HashMap<>();
    String roomName;
    List<String> adminNames = Arrays.asList("Tomasz", "Marek", "Dariusz", "Anna", "admin");

    public ChatRoom(String roomName) {
        this.roomName = roomName;
    }

    public static class idGenerator {
        private int currentCount;
        public static final idGenerator INSTANCE = new idGenerator(0);

        public idGenerator(int currentCount) {
            this.currentCount = currentCount;
        }

        public int getNewID(){
            return ++currentCount;
        }
    }

    public void userLogin(String nick){
        ChatUser c = new ChatUser(nick);
        c.setId(idGenerator.INSTANCE.getNewID());
        onlineUsers.put(c.getId(), c);
        if(adminNames.contains(c.getNick())) c.setAdmin(true);
        super.addObserver(c);
    }

    public void kickOut(int userId, int kickerId){
        if(adminNames.contains(onlineUsers.get(kickerId).getNick()) && onlineUsers.containsKey(userId)){
            ChatUser loser = onlineUsers.get(userId);
            super.deleteObserver(loser);
            System.out.println("ChatUser " + loser.getNick() + " has been kicked out by admin " + onlineUsers.get(kickerId).getNick());
            onlineUsers.remove(loser.getId());
        }
        else if (!onlineUsers.containsKey(userId)){
            System.out.println("Loser ain't here. Can't kick out nothin'!");
        }
        else System.out.println("You have no right to do that!");
    }

    public void sendMessage(int user, String text){
        Message message = new Message(user, text);
        System.out.println("User " + onlineUsers.get(message.getSenderID()).getNick() + " wrote: " + message.getText() + "\n");
        super.setChanged();
        super.notifyObservers(message);
    }

    public void sendPrivateMessage(int user, String text, int receiver){
        Message message = new Message(user, text, true, receiver);
        System.out.println("User " + onlineUsers.get(message.getSenderID()).getNick() + " wrote: " + message.getText() + "\n");
        super.setChanged();
        super.notifyObservers(message);
    }

}
