package Zadanie3;

import lombok.Getter;
import lombok.Setter;

import java.io.IOException;

@Getter
@Setter

public class Database {

    public static final Database INSTANCE = new Database();

    private Database() {
    }

    public void addRecord(Object object, DatabaseName databaseName) throws IOException {
        switch (databaseName){
            case DB_REQUESTS:{
                DatabaseName.DB_REQUESTS.getWriter().println(object.toString());
                DatabaseName.DB_REQUESTS.getWriter().flush();
                break;
            }

            case DB_ORDERS:{
                DatabaseName.DB_ORDERS.getWriter().println(object.toString());
                DatabaseName.DB_ORDERS.getWriter().flush();
                break;
            }

            case DB_SERVICE:{
                DatabaseName.DB_SERVICE.getWriter().println(object.toString());
                DatabaseName.DB_SERVICE.getWriter().flush();
                break;
            }
        }
    }
}
